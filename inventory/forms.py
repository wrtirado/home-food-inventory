from django.forms import ModelForm
from inventory.models import Food, Product, Category, Location

class FoodForm(ModelForm):
    class Meta:
        model = Food
        fields = [
            "food",
            "location",
            "exact_location",
            "category",
            "cost",
            "expiration",
        ]


class EditFoodForm(ModelForm):
    class Meta:
        model = Food
        fields = [
            "location",
            "exact_location",
            "remaining_servings",
        ]



class ProductForm(ModelForm):
    class Meta:
        model = Product
        fields = [
            "name",
            "total_servings",
            "calories_per_serving",
            "uom",
        ]



class CategoryForm(ModelForm):
    class Meta:
        model = Category
        fields = [
            "name",
        ]



class LocationForm(ModelForm):
    class Meta:
        model = Location
        fields = [
            "name",
        ]

