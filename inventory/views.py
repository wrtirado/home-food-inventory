from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from inventory.models import Food, Product
from inventory.forms import (
    FoodForm,
    EditFoodForm,
    ProductForm,
    CategoryForm,
    LocationForm,
)

# Create your views here.
@login_required
def inventory_list(request):
    food_list = Food.objects.filter(owner=request.user)
    context = {
        'food_list': food_list,
    }
    return render(request, 'inventory/list.html', context)


@login_required
def inventory_filter(request, filter):
    food_list = Food.objects.filter(owner=request.user).order_by(filter).reverse()
    context = {
        'food_list': food_list,
    }
    return render(request, 'inventory/list.html', context)


# Food Detail View
@login_required
def food_detail(request, id):
    food_item = Food.objects.get(id=id)
    context = {
        'food_item': food_item,
    }
    return render(request, 'inventory/detail.html', context)


# Edit Food View
@login_required
def edit_food(request, id):
    food = Food.objects.get(id=id)
    if request.method == "POST":
        form = EditFoodForm(request.POST, instance=food)
        if form.is_valid():
            form.save()
            return redirect('food_detail', id=id)
    else:
        form = EditFoodForm(instance=food)
    context = {
        'form': form,
        'food': food,
    }
    return render(request, 'inventory/edit-food.html', context)


# Add Inventory Item Views
@login_required
def add_food(request):
    if request.method == "POST":
        form = FoodForm(request.POST)
        if form.is_valid():
            food_item = form.save(False)
            parent_food = Product.objects.get(name=food_item.food)
            food_item.owner = request.user
            food_item.remaining_servings = parent_food.total_servings
            form.save()
            return redirect('inventory_list')
    else:
        form = FoodForm()
    context = {
        'form': form,
    }
    return render(request, 'inventory/add-food.html', context)


@login_required
# delete view for details
def delete_food(request, id):
    food_item = Food.objects.get(id=id)
    if request.method == "POST":
        # delete object
        food_item.delete()
        # after deleting redirect to
        # home page
        return redirect("inventory_list")



@login_required
def add_product(request):
    if request.method == "POST":
        form = ProductForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('inventory_list')
    else:
        form = ProductForm()
    context = {
        'form': form,
    }
    return render(request, 'inventory/add-product.html', context)


@login_required
def add_category(request):
    if request.method == "POST":
        form = CategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            form.save()
            return redirect('inventory_list')
    else:
        form = CategoryForm()
    context = {
        'form': form,
    }
    return render(request, 'inventory/add-category.html', context)


@login_required
def add_location(request):
    if request.method == "POST":
        form = LocationForm(request.POST)
        if form.is_valid():
            location = form.save(False)
            location.owner = request.user
            form.save()
            return redirect('inventory_list')
    else:
        form = LocationForm()
    context = {
        'form': form,
    }
    return render(request, 'inventory/add-location.html', context)
# END Add Inventory Item Views

# Edit Inventory Item Views
# @login_required
# def edit_food(request):
#     if request.method == "POST":
#         form = FoodForm(request.POST)
#         if form.is_valid():
#             food_item = form.save(False)
#             parent_food = Product.objects.get(name=food_item.food)
#             food_item.owner = request.user
#             food_item.remaining_servings = parent_food.total_servings
#             form.save()
#             return redirect('home')
#     else:
#         form = FoodForm()
#     context = {
#         'form': form,
#     }
#     return render(request, 'inventory/add-food.html', context)


# @login_required
# def edit_product(request):
#     if request.method == "POST":
#         form = ProductForm(request.POST)
#         if form.is_valid():
#             form.save()
#             return redirect('home')
#     else:
#         form = ProductForm()
#     context = {
#         'form': form,
#     }
#     return render(request, 'inventory/add-product.html', context)


# @login_required
# def edit_category(request):
#     if request.method == "POST":
#         form = CategoryForm(request.POST)
#         if form.is_valid():
#             category = form.save(False)
#             category.owner = request.user
#             form.save()
#             return redirect('home')
#     else:
#         form = CategoryForm()
#     context = {
#         'form': form,
#     }
#     return render(request, 'inventory/add-category.html', context)


# @login_required
# def edit_location(request):
#     if request.method == "POST":
#         form = LocationForm(request.POST)
#         if form.is_valid():
#             location = form.save(False)
#             location.owner = request.user
#             form.save()
#             return redirect('home')
#     else:
#         form = LocationForm()
#     context = {
#         'form': form,
#     }
#     return render(request, 'inventory/add-location.html', context)
