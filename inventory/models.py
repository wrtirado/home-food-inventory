from django.db import models
from django.conf import settings
from datetime import date
import datetime
from datetime import timedelta


# Create your models here.

class Category(models.Model):
    name = models.CharField(max_length=200)
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="categories",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name


class Location(models.Model):
    name = models.CharField(max_length=200)
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="locations",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name


class Product(models.Model):
    name = models.CharField(max_length=200)
    total_servings = models.SmallIntegerField()
    calories_per_serving = models.SmallIntegerField()
    uom = models.CharField(max_length=100)

    def __str__(self):
        return self.name
    

class Alternate_Name(models.Model):
    name = models.CharField(max_length=200)
    proper_name = models.ForeignKey(
        Product,
        related_name="alternate_names",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name
    

class Food(models.Model):
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="owned_foods",
        on_delete=models.CASCADE,
        null=True,
    )
    food = models.ForeignKey(
        Product,
        related_name="foods",
        on_delete=models.CASCADE,
    )
    location = models.ForeignKey(
        Location,
        related_name="food_locations",
        on_delete=models.CASCADE,
    )
    exact_location = models.CharField(max_length=200, default="N/A")
    category = models.ForeignKey(
        Category,
        related_name="food_categories",
        on_delete=models.CASCADE,
    )
    cost = models.FloatField()
    expiration = models.DateField(auto_now=False, auto_now_add=False)
    date_added = models.DateField(auto_now_add=True)
    remaining_servings = models.SmallIntegerField(null=True)

    def __str__(self):
        return self.food.name

    def cost_per_serving(self):
        return self.cost / self.food.total_servings
    
    def current_value(self):
        result = self.cost_per_serving() * self.remaining_servings
        return result
    
    def time_till_expiration(self):
        difference = self.expiration - date.today()
        return difference.days
    
    def total_calories(self):
        return self.food.calories_per_serving * self.food.total_servings
    
    def calories_per_serving(self):
        return self.food.calories_per_serving
    
    def remaining_calories(self):
        return self.calories_per_serving() * self.remaining_servings
