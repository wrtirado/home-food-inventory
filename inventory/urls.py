from django.urls import path
from inventory.views import (
    inventory_list,
    food_detail,
    edit_food,
    inventory_filter,
    add_food,
    add_product,
    add_category,
    add_location,
    delete_food,
)


urlpatterns = [
    path('', inventory_list, name='inventory_list'),
    path('<int:id>/', food_detail, name="food_detail"),
    path('<int:id>/edit/', edit_food, name="edit_food"),
    path('<str:filter>/', inventory_filter, name='inventory_filter'),
    path('add/food/', add_food, name="add_food"),
    path('add/product/', add_product, name="add_product"),
    path('add/category/', add_category, name="add_category"),
    path('add/location/', add_location, name="add_location"),
    path('delete/<int:id>/', delete_food, name="delete_food"),
]
