from django.contrib import admin
from inventory.models import (
    Category,
    Location,
    Product,
    Alternate_Name,
    Food,
)

# Register your models here.
@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = (
        "name",
    )


@admin.register(Location)
class LocationAdmin(admin.ModelAdmin):
    list_display = (
        "name",
    )


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "uom",
    )


@admin.register(Alternate_Name)
class Alternate_NameAdmin(admin.ModelAdmin):
    list_display = (
        "name",
    )


@admin.register(Food)
class FoodAdmin(admin.ModelAdmin):
    list_display = (
        "food",
        "location",
        "category",
        "cost",
        "expiration",
        "date_added",
    )
