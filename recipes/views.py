from django.shortcuts import render, redirect
from django.http import HttpResponse
from recipes.models import Recipe, Ingredient
from inventory.models import Food
from recipes.forms import RecipeForm, CookRecipeForm
from django.contrib.auth.decorators import login_required
from django.db.models import Sum



# Create your views here.
# Full List of Recipes View Function
@login_required
def recipe_list(request):
    recipes = Recipe.objects.all()
    context = {
        "page_title": "All Submitted Recipes",
        "recipe_list": recipes,
    }
    return render(request, 'recipes/list.html', context)

# User Specific Recipes View Function
@login_required
def my_recipe_list(request):
    recipes = Recipe.objects.filter(author=request.user)
    context = {
        "page_title": "Your Recipes",
        "recipe_list": recipes,
    }
    return render(request, 'recipes/list.html', context)


# Available Recipe List
@login_required
def available_recipes(request):
    recipes = Recipe.objects.filter(author=request.user)
    foods = Food.objects.filter(owner=request.user)
    print("Recipes:", recipes)
    has_all_ingredients = True
    available_recipes = []
    inventory_list = []
    for food in foods:
        inventory_list.append(str(food))
    print("Current Inventory List:", inventory_list)

    # iterate over the recipes list
    for recipe in recipes:
        # for each recipe, assign the recipe instance to a variable
        print("Recipe Object:", recipe)
        ingredients_needed = Ingredient.objects.filter(recipe=recipe)
        print("Needed Ingredients:", ingredients_needed)
        for ingredient in ingredients_needed:
            if str(ingredient) not in inventory_list:
                print(ingredient, "is not in", inventory_list)
                has_all_ingredients = False
        
        if has_all_ingredients:
            available_recipes.append(recipe)

    print("Available Recipes:", available_recipes)

    context = {
        'page_title': "Recipes Available With Current Food Inventory",
        'recipe_list': available_recipes,
    }

    return render(request, 'recipes/list.html', context)




# Specific Recipe Detail View Function
def show_recipe(request, id):
    recipe = Recipe.objects.get(id=id)
    if request.method == "POST":
        ingredients = recipe.ingredients.all()
        form = CookRecipeForm(request.POST)
        if form.is_valid():
            cook_count = form.cleaned_data['cook_count']
            if cook_count > 0:
                has_enough_ingredients = True
                for ingredient in ingredients:
                    food = ingredient.food_item
                    if cook_count > food.remaining_servings:
                        has_enough_ingredients = False
                    if has_enough_ingredients:
                        food.remaining_servings -= cook_count * ingredient.amount
                        food.save()
                    else:
                        form.add_error("cook_count", "You don't have enough ingredients to make the recipe this many times")
            else:
                form.add_error("cook_count", "You can't make a recipe zero or negative times... You goose.")
    else:
        form = CookRecipeForm()
        ingredients = recipe.ingredients.all()
    context = {
        "recipe_object": recipe,
        "form": form,
    }

    return render(request, 'recipes/detail.html', context)


# Cooked Recipe View Function
# def cook_recipe(request, id):
    
#     return render(request, 'recipes/create.html', context)


# Create View Function
@login_required
def create_recipe(request):
    if request.method == "POST":
        form = RecipeForm(request.POST)
        if form.is_valid():
            recipe = form.save(False)
            recipe.author = request.user
            form.save()
            return redirect("recipe_list")
    else:
        form = RecipeForm()
    context = {
        "form": form
    }
    return render(request, 'recipes/create.html', context)

# Edit View Function
def edit_recipe(request, id):
    recipe = Recipe.objects.get(id=id)
    if request.method =="POST":
        form = RecipeForm(request.POST, instance=recipe)
        if form.is_valid():
            form.save()
            return redirect("show_recipe", id=id)
    else:
        form = RecipeForm(instance=recipe)
    context = {
        "recipe_object": recipe,
        "recipe_form": form,
    }
    return render(request, "recipes/edit.html", context)

