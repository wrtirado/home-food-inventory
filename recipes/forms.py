from django import forms
from django.forms import ModelForm
from recipes.models import Recipe

class RecipeForm(ModelForm):
    class Meta:
        model = Recipe
        fields = [
            "title",
            "picture",
            "description",
            "rating",
        ]


class CookRecipeForm(forms.Form):
    cook_count = forms.IntegerField()
