from django.db import models
from django.conf import settings
from inventory.models import Food


# Create your models here.
class Recipe(models.Model):
    title = models.CharField(max_length=200)
    picture = models.URLField()
    description = models.TextField()
    created_on = models.DateTimeField(auto_now_add=True)
    rating = models.FloatField(null=True)

    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="recipes",
        on_delete=models.CASCADE,
        null=True
    )

    def __str__(self):
        return self.title
    
    def total_calories(self):
        total_calories = 0
        ingredients = self.ingredients.all()
        for ingredient in ingredients:
            total_calories += ingredient.food_item.food.calories_per_serving
        return total_calories
    
    def total_cost(self):
        total_cost = 0
        ingredients = self.ingredients.all()
        for ingredient in ingredients:
            total_cost += ingredient.food_item.cost / ingredient.food_item.food.total_servings
        return total_cost

class RecipeStep(models.Model):
    step_number = models.PositiveSmallIntegerField()
    instruction = models.TextField()
    recipe = models.ForeignKey(
        Recipe,
        related_name="steps",
        on_delete=models.CASCADE,
    )

    class Meta:
        ordering = ['step_number']

class Ingredient(models.Model):
    amount = models.SmallIntegerField(null=True)
    
    food_item = models.ForeignKey(
        Food,
        related_name="inventory_item",
        on_delete=models.CASCADE,
    )

    recipe = models.ForeignKey(
        Recipe,
        related_name="ingredients",
        on_delete=models.CASCADE,
    )

    class Meta:
        ordering = ['food_item']

    def __str__(self):
        return self.food_item.food.name